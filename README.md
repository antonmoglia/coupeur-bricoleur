![coupeur-bricoleur](documentation/images/coupeur-bricoleur-01.png)

# Coupeur Bricoleur

Bricoleur is a typographic system combining two weights of the Cooper Hewitt font family. By cutting the letters on the half, it became a kinf of multi-weight font. I named this system « Bricoleur » , french word for « handyman », because the graphic designer using the font have to fix the system himself to make it legible. After 3 years hidden in my computer, I decided to release a more user-friendly version: the cut versions were superimposed in a single file.

2016-2019.

## Specimen

![coupeur-bricoleur](documentation/images/specimen4.gif)

## License

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL

## Repository Layout

This font repository structure is inspired by [Unified Font Repository v0.3](https://github.com/unified-font-repository/Unified-Font-Repository).
