FONTLOG for Coupeur Bricoleur
-------------------

This file provides detailed information on the Coupeur Bricoleur font software.
This information should be distributed along with the Coupeur Bricoleur fonts
and any derivative works.


Basic Font Information
--------------------------

Bricoleur is a typographic system combining two weights of the Cooper Hewitt font family. By cutting the letters on the half, it became a kinf of multi-weight font. I named this system « Bricoleur » , french word for « handyman », because the graphic designer using the font have to fix the system himself to make it legible. After 3 years hidden in my computer, I decided to release a more user-friendly version: the cut versions were superimposed in a single file.


Information for Contributors
------------------------------

Coupeur Bricoleur is released under the OFL 1.1 - http://scripts.sil.org/OFL

For information on what you're allowed to change or modify, consult the
OFL-1.1.txt and OFL-FAQ.txt files. The OFL-FAQ also gives a very general
rationale and various recommendations regarding why you would want to
contribute to the project or make your own version of the font.


ChangeLog
----------

When you make modifications, be sure to add a description of your changes,
following the format of the other entries, to the start of this section.

20 June 2016 to 5 June 2019 (Anton) Coupeur Bricoleur, v1.001
- Changed name to "Coupeur Bricoleur"
Bricoleur is a typographic system combining two weights of the Cooper Hewitt font family. By cutting the letters on the half, it became a kinf of multi-weight font. I named this system « Bricoleur » , french word for « handyman », because the graphic designer using the font have to fix the system himself to make it legible.


25 June 2014 (Chester Jenkins) Cooper Hewitt v1.0
- Initial release of font "Cooper Hewitt"



Acknowledgements
-------------------------

When you make modifications, be sure to add your name (N), email (E),
web-address (W) and description (D). This list is sorted by last name in
alphabetical order.

N: Anton Moglia
E: anton@moglia.fr
W: moglia.fr
D: Forker
